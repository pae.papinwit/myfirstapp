import { Component, OnInit } from '@angular/core';
import { delay } from 'rxjs';
import { AuthTokenService } from '../../services/auth-token.service';
import { ChatService } from '../../services/chat.service';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit {
  OnlineUsers: any[] = [];
  myUser: string;
  newMessage: string;
  currentChatHeader = '';
  roomAndMsg: any = [];
  //roomAndMsg: [{ rid: string; msg: string[] }] = [{ rid: '', msg: [] }];
  messagebox: string[] = [];

  constructor(private chatService: ChatService, private authToken: AuthTokenService) {
    this.chatService.testReceived().subscribe((data) => {
      this.storeAlignData(data);
      var temp_rid = this.findRoomID(this.currentChatHeader, this.getRoomList());
      //Update Messagebox
      this.messagebox = this.getMsgBoxbyRID(temp_rid);

      console.log(this.messagebox);
    });
  }

  ngOnInit() {
    this.myUser = this.authToken.getUser().email;
    this.chatService.setupSocketConnection();
  }

  storeAlignData(data) {
    //console.log('Head: ');
    //console.log(data);
    var rid = data.rid;
    var mssg = data.msg;
    var messageStore: string[] = [];
    messageStore.push(mssg);
    // console.log('messageStore: ');
    // console.log(messageStore);
    var rIDandMsg = { rid: rid, msg: messageStore };
    // console.log('rIDandMsg: ');
    // console.log(rIDandMsg);
    var Index = this.roomAndMsg.findIndex((obj) => {
      // console.log('objIndex: ' + obj.rid);
      return obj.rid == rid;
    });
    // console.log('index: ' + Index);
    if (Index === -1) {
      this.roomAndMsg.push(rIDandMsg);
    } else {
      this.roomAndMsg[Index].msg.push(messageStore);
    }
    // console.log('Store: ');
    // console.log(this.roomAndMsg);
  }

  getMsgBoxbyRID(rid) {
    var messageStore: string[] = [];
    var Index = this.roomAndMsg.findIndex((obj) => {
      return obj.rid == rid;
    });
    if (Index !== -1) {
      messageStore = this.roomAndMsg[Index].msg;
      return messageStore;
    } else {
      return messageStore;
    }
  }

  /* +++++++++++++++++
  async getMessage() {
    let themessage: any = await this.chatService.testsubscribeToMessages();
    // let themessage = this.chatService.subscribeToMessages();
    console.log('init: ' + themessage);
    console.log(' messagebox : ' + this.messagebox);

    if (themessage !== '') {
      this.messagebox.push(themessage);
      console.log('this.messagebox >>' + this.messagebox);
    }
  }*/

  getOnlineUsers() {
    var OnlineUsers = this.chatService.getOnlineUser();
    var usersIndex = OnlineUsers.findIndex((obj) => {
      return obj.id === this.getMyUser().id;
    });
    if (usersIndex !== -1) {
      OnlineUsers.splice(usersIndex, 1);
    }
    return OnlineUsers;
  }

  getMyUser() {
    return this.authToken.getUser();
  }

  getRoomList() {
    return this.genChatRoom(this.getMyUser().email, this.getOnlineUsers());
  }

  genChatRoom(myuser, allusers) {
    var rooms = [];
    for (let i = 0; i < allusers.length; i++) {
      let roomid = '';
      if (myuser > allusers[i].user) {
        roomid = myuser + '_' + allusers[i].user;
      } else {
        roomid = allusers[i].user + '_' + myuser;
      }
      var roomsIndex = rooms.findIndex((obj) => {
        return obj == roomid;
      });
      if (roomsIndex === -1) {
        rooms.push({ user: allusers[i].user, rid: roomid });
      }
    }
    return rooms;
  }

  findRoomID(targetuser, allroomID) {
    var roomID = '';
    var thisuser = targetuser.trim();
    var roomsIndex = allroomID.findIndex((obj) => {
      return obj.user === thisuser;
    });
    if (roomsIndex !== -1) {
      roomID = allroomID[roomsIndex].rid;
    }
    if (roomID == '') {
      return null;
    }
    return roomID;
  }

  onClick(e) {
    this.currentChatHeader = e.target.innerHTML;
    this.chatService.joinRoom(this.findRoomID(e.target.innerHTML.trim(), this.getRoomList()));
    this.messagebox = this.getMsgBoxbyRID(this.findRoomID(e.target.innerHTML.trim(), this.getRoomList()));
  }

  onClickSend() {
    var thisroomID = this.findRoomID(this.currentChatHeader, this.getRoomList());
    var combineMsg = this.getMyUser().email.trim() + ': ' + this.newMessage;
    this.chatService.sendMessage({ rid: thisroomID, msg: combineMsg });
    this.newMessage = '';
  }
}
