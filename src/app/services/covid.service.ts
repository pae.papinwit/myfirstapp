import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CovidService {
  constructor(private http: HttpClient) {}

  getData() {
    return this.http.get('https://covid19.ddc.moph.go.th/api/Cases/today-cases-all');
  }
  getHistData() {
    return this.http.get('https://covid19.ddc.moph.go.th/api/Cases/timeline-cases-all');
  }
  getDataPerProvinces() {
    return this.http.get('https://covid19.ddc.moph.go.th/api/Cases/today-cases-by-provinces');
  }
}
